![](https://elioway.gitlab.io/elioangels/daemon/elio-daemon-logo.png)

> Demoniack phrenzy, **the elioWay**

# daemon ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Help using Docker and kubernetes.

- [daemon Documentation](https://elioway.gitlab.io/elioangels/daemon/)

## Installing

- [Installing daemon](https://elioway.gitlab.io/elioangels/daemon/installing.html)

## Requirements

- [elioangels Prerequisites](https://elioway.gitlab.io/elioangels/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
minikube start

```

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [daemon Quickstart](https://elioway.gitlab.io/elioangels/daemon/quickstart.html)

# Credits

- [daemon Credits](https://elioway.gitlab.io/elioangels/daemon/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioangels/daemon/apple-touch-icon.png)
