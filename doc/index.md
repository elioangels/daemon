<aside>
  <dl>
  <dd>Convulsions, epilepsies, fierce catarrhs,</dd>
  <dd>Intestine stone and ulcer, colick-pangs,</dd>
  <dd>Demoniack phrenzy, moaping melancholy,</dd>
  <dd>And moon-struck madness, pining atrophy</dd>
</dl>
</aside>

A gathering of scripts, notes, files and resources for using Docker, Kubes, etc

# Cheat

- [docker](cheat-docker.html)
- [docker-compose](cheat-docker-compose.html)
- [draft](cheat-draft.html)
- [kubectl](cheat-kubectl.html)
- [minikube](cheat-kubectl.html)
- [skaffold](cheat-kubectl.html)
- [Quickstart mongodb-docked](quickstart-mongodb-docked.html)
