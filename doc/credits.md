# daemon Credits

## Core Thanks!

- [bitnami](https://bitnami.com/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)

## Useful

- [bitnami/get-started-kubernetes](https://docs.bitnami.com/kubernetes/get-started-kubernetes/)
- [bitnami/deploy-rest-api-nodejs-mongodb-charts/](https://docs.bitnami.com/tutorials/deploy-rest-api-nodejs-mongodb-charts/)
- [kubernetes/developing-on-kubernetes](https://kubernetes.io/blog/2018/05/01/developing-on-kubernetes/)

## Experimenting

- [azure/draft](https://github.com/Azure/draft)
- [GoogleContainerTools/skaffold](https://github.com/GoogleContainerTools/skaffold)

## Artwork

- <https://publicdomainvectors.org/en/free-clipart/Huntingdonshire-horn-silhouette/62952.html>
- <https://commons.wikimedia.org/wiki/File:Timothy_Cole_-_Old_Italian_Masters-_Angel_Sounding_a_Trumpet_-_1932.575_-_Cleveland_Museum_of_Art.jpg>
