# Quickstart daemon

- [daemon Prerequisites](/elioangels/daemon/prerequisites.html)
- [Installing daemon](/elioangels/daemon/installing.html)

## Nutshell

Using minikube in development which runs a cluster inside a Docker container.

1. Minikube starts the Kubernetes service

<https://minikube.sigs.k8s.io/docs/start/>

```shell
minikube start
```

## Overview

**minikube** will run the Development cluster in Docker for us. Every session will begin with `minikube start`.

```shell
minikube start
```

- [Cheat docker](/cheat-docker.html)
- [Cheat minikube](/cheat-minikube.html)
- [Cheat docker-compose](/cheat-docker-compose.html)
- [Cheat kubectl](/cheat-kubectl.html)
- [Cheat docker](/cheat-docker.html)
- [Cheat draft](/cheat-draft.html)
- [Cheat skaffold](/cheat-skaffold.html)
