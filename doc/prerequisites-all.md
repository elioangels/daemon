# daemon Prerequisites

- [elioangels Prerequisites](/elioangels/prerequisites.html)

# Docker, Kubernetes, Helm, k3d

You'll need this if you plan to contribute to **the elioWay**. It's not compulsory but then you're on your own setting up servers.

## **docker**

```shell
sudo apt install docker.io virtualbox -y
usermod -aG docker tim
docker version
> v19.xx.xx
sudo service --status-all
```

## **kubectl**

```shell
echo (curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
> v1.19.4
curl -LO "https://storage.googleapis.com/kubernetes-release/release/v1.19.4/bin/linux/amd64/kubectl"
# Unzip to bin
chmod +x bin/kubectl
kubectl version
> v1.19.4
```

## **minikube**

```shell
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube version
> minikube version: v1.15.0
> commit: 3e098ff146b8502f597849dfda420a2fa4fa43f0
```

- <https://docs.bitnami.com/kubernetes/get-started-kubernetes/>

## **Helm**

```shell
wget https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz
# Unzip to bin
chmod +x bin/helm
helm version
> v3.xx.xx
```

## **draft**

```shell
wget https://azuredraft.blob.core.windows.net/draft/draft-canary-linux-amd64.tar.gz
# Unzip to bin
chmod +x bin/draft
draft version
> v3.xx.xx
draft init
```

## **skaffold**

```shell
wget https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
chmod +x skaffold
mv skaffold ~/bin/
skaffold version
> docker-compose version 1.27.4, build 40524192
> docker-py version: 4.3.1
> CPython version: 3.7.7
> OpenSSL version: OpenSSL 1.1.0l  10 Sep 2019
```

### **docker-compose**

```shell
wget https://github.com/docker/compose/releases/download/1.27.4/docker-compose-Darwin-x86_64.tgz
mv docker-compose-Darwin-x86_64 docker-compose
chmod +x docker-compose
mv docker-compose ~/bin/
docker-compose version
> docker-compose version
docker-compose init
```

- <https://minikube.sigs.k8s.io/docs/start/>
