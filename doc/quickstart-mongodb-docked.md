# Quickstart MongoDb daemon

<https://docs.bitnami.com/tutorials/deploy-rest-api-nodejs-mongodb-charts/>

## Setup

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
> "bitnami" has been added to your repositories
```

## Install New

```shell
helm install eliomongodb bitnami/mongodb \
  --set mongodbRootPassword=rootmein \
  --set mongodbUsername=eliouser \
  --set mongodbPassword=letmein \
  --set mongodbDatabase=eliodb \
  --set replicaSet.enabled=true
> NAME: eliomongodb
> LAST DEPLOYED: Sat Nov 14 23:21:28 2020
> NAMESPACE: default
> STATUS: deployed
> REVISION: 1
> TEST SUITE: None
> NOTES:
> ** Please be patient - the chart is being deployed **

helm uninstall eliomongodb
```

## Usage

MongoDB can be accessed via port 27017 on the following DNS name(s) from within your cluster:

```
mongodb.default.svc.cluster.local
```

To get the root password run:

```shell
set -U MONGODB_ROOT_PASSWORD (kubectl get secret --namespace default eliomongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
#bash
export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace default eliomongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
```

To connect to your database, create a MongoDB client container:

```shell
kubectl run --namespace default mongodb-client --rm --tty -i --restart='Never' --image docker.io/bitnami/mongodb:4.4.1-debian-10-r61 --command -- bash
```

Then, run the following command:

```shell
mongo admin --host "eliomongodb" --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD
```

To connect to your database from outside the cluster execute the following commands:

```shell
kubectl port-forward --namespace default svc/mongodb 27017:27017 &
mongo --host 127.0.0.1 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD
```

## Accessing the MongoDB from your App.

In your app, modify the MongoDb connection string to look like this:

```javascript
let CNN = `mongodb://${DATABASE_USER}:${DATABASE_PASSWORD}@${DATABASE_HOST}/${DATABASE_NAME}`
```

Modify the `start` action in `package.json`

```
{
  ...
  "scripts": {
    "start": "node app.js",
  ...
  }
}
```
