# daemon Prerequisites

- [elioangels Prerequisites](/elioangels/prerequisites.html)

# Docker, Kubernetes, Helm, k3d

You'll need this if you plan to contribute to **the elioWay**. It's not compulsory but then you're on your own setting up servers.

## **docker**

```shell
sudo apt install docker.io virtualbox -y
usermod -aG docker tim
docker version
> v19.xx.xx
sudo service --status-all
```

## **minikube**

```shell
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube version
> minikube version: v1.15.0
> commit: 3e098ff146b8502f597849dfda420a2fa4fa43f0
```

- <https://docs.bitnami.com/kubernetes/get-started-kubernetes/>

### **docker-compose**

```shell
wget https://github.com/docker/compose/releases/download/1.27.4/docker-compose-Darwin-x86_64.tgz
mv docker-compose-Darwin-x86_64 docker-compose
chmod +x docker-compose
mv docker-compose ~/bin/
docker-compose version
> docker-compose version
docker-compose init
```

- <https://minikube.sigs.k8s.io/docs/start/>
